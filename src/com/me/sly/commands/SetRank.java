package com.me.sly.commands;


import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.me.sly.PermissionsMechanics.Rank;
import com.me.sly.PermissionsMechanics.RankAPI;
import com.me.sly.UHC.Main.ChatAPI;

public class SetRank implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){

        Player p = (Player) sender;

        if(cmd.getName().equalsIgnoreCase("setrank")) {

            if(RankAPI.getRank(p).getRankValue() > 3) {

                if(args.length == 0) {
                    p.sendMessage(ChatAPI._PERMSTAG + "Example usage:    /setrank <player> <rank (USER, OWNER)>");
                    return false;
                }

                Player target = Bukkit.getServer().getPlayer(args[0]);
                Rank rank = Rank.getRankFromString(args[1]);


                RankAPI.setRank(target, rank);
                p.sendMessage(ChatAPI._PERMSTAG + "You set: " + target.getName() + "'s rank to: " + rank);
                target.sendMessage(ChatAPI._PERMSTAG + "You rank was set to: " + rank);

                return true;


            } else {
                p.sendMessage(ChatAPI._NOPERMS);
                return true;
            }




        }


        return false;
    }
}
