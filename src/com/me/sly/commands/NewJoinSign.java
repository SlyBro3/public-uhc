package com.me.sly.commands;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.me.sly.PermissionsMechanics.RankAPI;
import com.me.sly.UHC.Main.ChatAPI;
import com.me.sly.UHC.Main.UHC;
import com.me.sly.UHC.Main.JoinSigns.JoinSign;
import com.me.sly.UHC.Main.JoinSigns.LoadJoinSigns;

public class NewJoinSign implements CommandExecutor{
	@Override
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String cmdL,
			String[] args) {
		if(sender instanceof Player){
			Player p = (Player) sender;
			if(RankAPI.getRank(p).getRankValue() > 3){
			Block target = p.getTargetBlock(null, 5);
			if(target == null){
				ChatAPI.messagePlayer(p, "You are not looking at a block to make a join sign!");
				return true;
			}
			Block block =  target;
			block.setType(Material.SIGN);
			JoinSign joinSign = new JoinSign((Sign) block, block.getWorld());
			UHC.plugin.joinSigns.add(joinSign);
			LoadJoinSigns.save(joinSign);
		}
		}
		return false;
	}
}
