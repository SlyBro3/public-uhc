package com.me.sly.commands;


import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.me.sly.PermissionsMechanics.RankAPI;
import com.me.sly.UHC.Main.ChatAPI;

public class GetRank implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player p = (Player) sender;

        if (cmd.getName().equalsIgnoreCase("getrank")) {

            if (RankAPI.getRank(p).getRankValue() > 2) { //BOTH ADMINS AND OWNERS CAN USE THIS COMMAND

                if (args.length == 0) {
					ChatAPI.cleanMessagePlayer(p, ChatAPI._PERMSTAG + "Example usage:    /getrank <player>");
                    return false;
                }

                Player target = Bukkit.getServer().getPlayer(args[0]);

                String rank = RankAPI.getRank(target).getFormat();

                ChatAPI.cleanMessagePlayer(p, ChatAPI._PERMSTAG + target.getName() + "'s rank is: " + rank);
                return true;
            } else {
                ChatAPI.cleanMessagePlayer(p, ChatAPI._NOPERMS);
                return true;
            }
        }

        return false;
    }


}
