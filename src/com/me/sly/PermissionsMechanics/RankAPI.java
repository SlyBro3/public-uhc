package com.me.sly.PermissionsMechanics;


import org.bukkit.entity.Player;

public class RankAPI {
	


    public static void setRank(Player p, Rank r) {
		Config c = new Config(p);
		c.set("Permissions.rank", r.getFormat());
    }

    public static boolean hasRank(Player p) {
       return new Config(p).getString("Permissions.rank") != null;
    }

	public static Rank getRank(Player p) {
		Config c = new Config(p);
		if (hasRank(p)) {
			if (c.getString("Permissions.rank").equalsIgnoreCase("banned")) {
				return Rank.BANNED;
			}
			if (c.getString("Permissions.rank").equalsIgnoreCase("user")) {
				return Rank.USER;
			}
			if (c.getString("Permissions.rank").equalsIgnoreCase("supporter")) {
				return Rank.SUPPORTER;
			}
			if (c.getString("Permissions.rank").equalsIgnoreCase("mod")) {
				return Rank.MOD;
			}
			if (c.getString("Permissions.rank").equalsIgnoreCase("admin")) {
				return Rank.ADMIN;
			}
			if (c.getString("Permissions.rank").equalsIgnoreCase("owner")) {
				return Rank.OWNER;
			}
		}
		c.set("Permissions.rank", "user");

		return Rank.USER;
	}
}

