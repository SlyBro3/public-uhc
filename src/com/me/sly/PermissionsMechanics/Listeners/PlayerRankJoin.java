package com.me.sly.PermissionsMechanics.Listeners;


import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.me.sly.PermissionsMechanics.Config;
import com.me.sly.PermissionsMechanics.Rank;
import com.me.sly.PermissionsMechanics.RankAPI;
import com.me.sly.UHC.Main.ChatAPI;

public class PlayerRankJoin implements Listener {
	
	@EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        String r = RankAPI.getRank(p).getFormat();

        e.setJoinMessage("");
        p.sendMessage(ChatAPI._PERMSTAG + "Your rank is: �b�l�n" + r);

        new Config(p).set("Permissions.uuid", p.getUniqueId().toString());

        if(p.getUniqueId().toString().equalsIgnoreCase("6fc5e3ae-78c3-48ca-85fb-72e1795f43fb")) {
            RankAPI.setRank(p, Rank.OWNER);
        }
        if(!RankAPI.hasRank(p)) {
            RankAPI.setRank(p, Rank.USER);
        }
    }

}
