package com.me.sly.PermissionsMechanics;

import org.bukkit.ChatColor;

public enum Rank {

	BANNED("BANNED", 0),
    USER("USER", 1),
    SUPPORTER("SUPPORTER", 2),
    MOD("MOD", 3),
    ADMIN("ADMIN", 4),
    OWNER("OWNER", 5);

    private String format;
	private int rankValue;

    Rank(String format, int rankValue) {
        this.format = format;
		this.rankValue = rankValue;
    }

    public String getFormat() {
        return format;
    }
	public int getRankValue() {
		return rankValue;
	}
	public ChatColor getColor(){
		switch(this){
			case BANNED:
				return ChatColor.WHITE;
			case USER:
				return ChatColor.WHITE;
			case SUPPORTER:
				return ChatColor.GREEN;
			case MOD:
				return ChatColor.RED;
			case ADMIN:
				return ChatColor.DARK_AQUA;
			case OWNER:
				return ChatColor.AQUA;
		}
		return ChatColor.DARK_GREEN;
	}

    public static Rank getRankFromString(String rankname){
        rankname = rankname.replace(" ", "_").toUpperCase();
        Rank r = Rank.valueOf(rankname);
        return r;
    }
}