package com.me.sly.PermissionsMechanics;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.me.sly.UHC.Main.UHC;

public class Config {
    private UUID playerUUID;
    File f;
    YamlConfiguration yl;
    public Config(Player player){
        this.playerUUID = player.getUniqueId();
        f = new File(UHC.plugin.getDataFolder(),  "Players");
        f.mkdir();
        f = new File(UHC.plugin.getDataFolder() + "/Players",  playerUUID.toString() + ".yml");
        yl = YamlConfiguration.loadConfiguration(f);

    }


    public boolean doesExist(){
        return f.exists();
    }
    public Config create(){
        try {
            if(doesExist() == false) {
                f.createNewFile();

            }
        } catch (IOException e) {
            e.printStackTrace();}
        save();
        return this;
    }
    public YamlConfiguration getYaml(){
        return yl;
    }
    public void set(String path, Object o){
        yl.set(path, o);
        save();
    }
    public String getString(String path){
        return yl.getString(path);
    }

    public int getInt(String path){
        return yl.getInt(path);
    }
    public boolean getBoolean(String path){
        return yl.getBoolean(path);
    }
    public ItemStack getItemStack(String path){
        return yl.getItemStack(path);
    }
    @SuppressWarnings("unchecked")
    public List<ItemStack> getItemStackList(String path){
        return (List<ItemStack>) yl.getList(path);
    }
    public void save(){
        try {yl.save(f);} catch (IOException e) {}
    }
}
