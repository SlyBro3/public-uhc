package com.me.sly.UHC.Main;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.ScoreboardManager;

import com.me.sly.UHC.Main.GameMechanics.Game;
import com.me.sly.UHC.Main.JoinSigns.JoinSign;
import com.me.sly.UHC.Main.JoinSigns.LoadJoinSigns;

public class UHC extends JavaPlugin implements Listener{
	public static UHC plugin;
	public static List<String> alivePlayers = new ArrayList<String>();
	public static ScoreboardManager sm = Bukkit.getScoreboardManager();
	public ArrayList<Game> games;
	public ArrayList<JoinSign> joinSigns;
	public void onEnable(){
		plugin = this;
		Bukkit.setSpawnRadius(0);
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "Gamerule naturalRegeneration false");
		Bukkit.getPluginManager().registerEvents(this, this);
		LoadJoinSigns.loadJoinSigns();
		LoadJoinSigns.spawnJoinSigns();
		EnableMethods.registerListeners();
		EnableMethods.registerCommands();
	}
	public void onDisable(){
		plugin = null;
	}
	public int rand(int min, int max) {
		max += 1;
        return min + (new Random()).nextInt(max-min);
    }
	
	}
