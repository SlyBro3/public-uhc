package com.me.sly.UHC.Main.GameMechanics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.me.sly.UHC.Main.UHC;
import com.me.sly.UHC.Main.JoinSigns.JoinSign;
import com.me.sly.UHC.Main.UHCObjects.UHCTeam;

public class GameLobbyListeners implements Listener{

	@EventHandler
	public void onGameJoin(PlayerInteractEvent event){
		if(event.getClickedBlock().getType() == Material.SIGN_POST || event.getClickedBlock().getType() == Material.WALL_SIGN){
			if(!JoinSign.isJoinSign((Sign) event.getClickedBlock().getState())){
				return;
			}
			JoinSign joinSign = JoinSign.getJoinSignFor((Sign) event.getClickedBlock().getState());
			event.getPlayer().teleport(new Location(joinSign.getWorld(), 0, 100, 0));
			joinSign.updateSign();
			if(joinSign.getWorld().getPlayers().size() == 8){
				List<Player> players = joinSign.getWorld().getPlayers();
				
				UHCTeam m1 = new UHCTeam(Arrays.asList(players.get(0).getName(), players.get(1).getName()), "BLUE");
				UHCTeam m2 = new UHCTeam(Arrays.asList(players.get(2).getName(), players.get(3).getName()), "RED");
				UHCTeam m3 = new UHCTeam(Arrays.asList(players.get(4).getName(), players.get(5).getName()), "GREEN");
				UHCTeam m4 = new UHCTeam(Arrays.asList(players.get(6).getName(), players.get(7).getName()), "YELLOW");
				ArrayList<UHCTeam> teams = new ArrayList<UHCTeam>();
				teams.add(m1);
				teams.add(m2);
				teams.add(m3);
				teams.add(m4);
				final Game g = new Game(UHC.plugin, joinSign.getWorld(), 1000, teams);
				UHC.plugin.games.add(g);
				new BukkitRunnable(){
					public void run(){
						g.startGame();
					}
				}.runTaskLater(UHC.plugin, 10*20);
			}
		}
	}
}
