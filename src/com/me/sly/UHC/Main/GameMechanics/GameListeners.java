package com.me.sly.UHC.Main.GameMechanics;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.me.sly.UHC.Main.ChatAPI;
import com.me.sly.UHC.Main.UHC;
import com.me.sly.UHC.Main.UHCObjects.UHCTeam;

public class GameListeners implements Listener{

		@EventHandler
		public void onQuit(PlayerQuitEvent event){
			if(event.getPlayer().getWorld().getName().contains("gameWorld_")){
			event.setQuitMessage("");
			Game g = Game.getGameFromWorld(event.getPlayer().getWorld());
			ChatAPI.broadcastGameMessage(event.getPlayer().getName() + " has quit from your game!", g.getWorld(), false);
			g.alivePlayers.remove(event.getPlayer());
			}
		}
		@EventHandler
		public void onJoin(PlayerJoinEvent event){
				event.setJoinMessage("");
					event.getPlayer().teleport(new Location(Bukkit.getWorld("world"), UHC.plugin.getConfig().getInt("SpawnLoc.X"),UHC.plugin.getConfig().getInt("SpawnLoc.Y"),UHC.plugin.getConfig().getInt("SpawnLoc.Z")));
		}
		@EventHandler
		public void onFoodChange(FoodLevelChangeEvent event){
			if(event.getEntity().getWorld().getName().contains("gameWorld_")){
				Game g = Game.getGameFromWorld(event.getEntity().getWorld());
			if(!(g.getGameState() == GameState.INGAME && g.getGameState() == GameState.GRACE_PERIOD && g.getGameState() == GameState.DEATHMATCH)){
				event.setFoodLevel(20);
				return;
			}
				int i = UHC.plugin.rand(1, 3);
				if(i == 1){
					event.setCancelled(true);
				
			}
		}
		}
		@EventHandler
		public void onMove(PlayerMoveEvent event){
			if(event.getPlayer().getWorld().getName().contains("gameWorld_")){
				Game g = Game.getGameFromWorld(event.getPlayer().getWorld());
				if(g.getGameState() == GameState.PREGAME || g.getGameState() == GameState.PRE_DEATHMATCH || g.getGameState() == GameState.STARTUP){
			if(event.getFrom().getBlockX() != event.getTo().getBlockX() || event.getFrom().getBlockZ() != event.getTo().getBlockZ()){
				event.getPlayer().teleport(new Location(event.getFrom().getWorld(), event.getFrom().getBlockX(), event.getFrom().getBlockY(), event.getFrom().getBlockZ()));
			}
		}
		}
		}
		@SuppressWarnings("unchecked")
		@EventHandler
		public void onDeath(PlayerDeathEvent event){
			if(event.getEntity().getWorld().getName().contains("gameWorld_")){
				Game g = Game.getGameFromWorld(event.getEntity().getWorld());
				ChatAPI.broadcastGameMessage(event.getDeathMessage(), g.getWorld(), false);
				g.alivePlayers.remove(event.getEntity());
				ArrayList<UHCTeam> aliveTeams = new ArrayList<UHCTeam>();
				ArrayList<String> alivePlayers = (ArrayList<String>) g.alivePlayers.clone();
				for(String gr : alivePlayers){
					if(!aliveTeams.contains(UHCTeam.getTeamFor(g, Bukkit.getPlayer(gr)))) aliveTeams.add(UHCTeam.getTeamFor(g, Bukkit.getPlayer(gr)));
				}
				if(aliveTeams.size() == 1){
					ChatAPI.broadcastMessage("The " + aliveTeams.get(0).getName() + " team has won the UHC on " + g.getWorld().getName(), 0, true);
					g.setGameState(GameState.END);
					UHC.plugin.games.remove(g);
					for(String gr : alivePlayers){
						Bukkit.getPlayer(gr).teleport(new Location(Bukkit.getWorld("world"), UHC.plugin.getConfig().getInt("SpawnLoc.X"),UHC.plugin.getConfig().getInt("SpawnLoc.Y"),UHC.plugin.getConfig().getInt("SpawnLoc.Z")));
						Bukkit.getPlayer(gr).setHealth(20.0);
						Bukkit.getPlayer(gr).getInventory().clear();
						Bukkit.getPlayer(gr).getActivePotionEffects().clear();
					}
				}
				}
		}
		@EventHandler
		public void onDamage(final EntityDamageEvent event){
			if(event.getEntity() instanceof Player){
			if(event.getEntity().getWorld().getName().contains("gameWorld_")){
				Game g = Game.getGameFromWorld(event.getEntity().getWorld());
				if(!(g.getGameState() == GameState.INGAME && g.getGameState() == GameState.DEATHMATCH && g.getGameState() == GameState.GRACE_PERIOD)){
				event.setCancelled(true);
				event.setDamage(0.0);
				return;
				}

			}else{
				event.setDamage(0.0);
				event.setCancelled(true);
				return;
			}
			}
		}
		@EventHandler
		public void onPlayerDamage(final EntityDamageByEntityEvent event){
			if(event.getEntity() instanceof Player){
			if(event.getEntity().getWorld().getName().contains("gameWorld_")){
				Game g = Game.getGameFromWorld(event.getEntity().getWorld());
				if((g.getGameState() == GameState.GRACE_PERIOD && event.getDamager() instanceof Player)){
					event.setCancelled(true);
					event.setDamage(0.0);
					return;
				}
				if(event.getDamager() instanceof Player && UHCTeam.getTeamFor(g, (Player) event.getDamager()) == UHCTeam.getTeamFor(g, (Player) event.getEntity())){
					event.setCancelled(true);
					event.setDamage(0.0);
					return;
				}
			}
			}
		}
		@EventHandler
		public void onRespawn(PlayerRespawnEvent event){
				event.getPlayer().teleport(new Location(Bukkit.getWorld("world"), UHC.plugin.getConfig().getInt("SpawnLoc.X"),UHC.plugin.getConfig().getInt("SpawnLoc.Y"),UHC.plugin.getConfig().getInt("SpawnLoc.Z")));
		}
}
