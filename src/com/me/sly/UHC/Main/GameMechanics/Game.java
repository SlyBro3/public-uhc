package com.me.sly.UHC.Main.GameMechanics;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.me.sly.UHC.Main.ChatAPI;
import com.me.sly.UHC.Main.UHC;
import com.me.sly.UHC.Main.UHCObjects.UHCTeam;

public class Game {

	private UHC main;
	private World world;
	private int border;
	private GameState gameState = GameState.PREGAME;
	private ArrayList<UHCTeam> teams;
	public ArrayList<String> alivePlayers = new ArrayList<String>();
	private Scoreboard s = UHC.sm.getNewScoreboard();
	private Objective health;
	private int countdownInt = 30;
	public Game(UHC main, World world, int border, ArrayList<UHCTeam> teams){
		this.main = main;
		this.world = world;
		this.border = border;
		this.health = s.getObjective("Health");
		this.teams = teams;
		for(UHCTeam t : teams){
			for(String p : t.getPlayers())
			alivePlayers.add(p);
		}
	}
	public void startGame(){
		for(UHCTeam team : teams){
			int randX = main.rand(0, border*2);
			randX-=border;
			int randZ = main.rand(0, border*2);
			randZ-=border;
			for(String g : team.getPlayers()){
				Player p = Bukkit.getPlayer(g);
				if(p != null) p.teleport(new Location(world, randX, 100, randZ));
			}
			
		}
		gameState = GameState.STARTUP;
		new BukkitRunnable(){
			public void run(){
				if(countdownInt == 30) ChatAPI.broadcastGameMessage("UHC Starting in 30 seconds, get ready", world, true);
				else if(countdownInt == 20) ChatAPI.broadcastGameMessage("UHC Starting in 20 seconds, get ready", world, true);
				else if(countdownInt <= 10 && countdownInt != 0) ChatAPI.broadcastGameMessage("UHC Starting in " + countdownInt + " seconds, get ready", world, true);
				else if(countdownInt == 0){
					ChatAPI.broadcastGameMessage("The UHC has begun! There will be a 10 minute grace period.", world, true);
					ChatAPI.broadcastGameMessage("Logging out will result in a kick from the current game.", world, true);
					gameState = GameState.GRACE_PERIOD;
					for(String sr : alivePlayers){
						Player p = Bukkit.getPlayer(sr);
						p.setScoreboard(s);
					}
					this.cancel();
				}
				countdownInt--;
			}
		}.runTaskTimer(main, 0, 20);
		new BukkitRunnable(){
			public void run(){
				ChatAPI.broadcastGameMessage("The 10 minute grace period has ended! 50 minutes until deathmatch.", world, true);
				gameState = GameState.INGAME;
			}
		}.runTaskLater(UHC.plugin, 10*60*20);
		new BukkitRunnable(){
			public void run(){
				if(gameState != GameState.END){
				gameState = GameState.PRE_DEATHMATCH;
				ChatAPI.broadcastGameMessage("Deathmatch beginning in 10 seconds; Get ready!", world, true);
				for(String g : alivePlayers){
					Player pl = Bukkit.getPlayer(g);
					pl.teleport(new Location(Bukkit.getWorld("world"), 0, 100, 0));
					new BukkitRunnable(){
						public void run(){
							gameState = GameState.DEATHMATCH;
							ChatAPI.broadcastGameMessage("The deathmatch has begun!", world, true);
							ChatAPI.broadcastGameMessage("Your invinciblity has worn off, good luck!", world, true);
						}
					}.runTaskLater(UHC.plugin, 10*20);
				}
			}
			}
		}.runTaskLater(UHC.plugin, 60*60*20);
	}
	public ArrayList<String> getAlivePlayers(){
		return alivePlayers;
	}
	public void resetWorld(){
		//TODO Reset the world of the game just played
	}
	public GameState getGameState(){
		return gameState;
	}
	public void setGameState(GameState gameState){
		this.gameState = gameState;
	}
	@SuppressWarnings("unchecked")
	public ArrayList<UHCTeam> getTeams(){
		return (ArrayList<UHCTeam>) teams.clone();
	}
	public int getBorder(){
		return border;
	}
	public Objective getHealthObjective(){
		return health;
	}
	public World getWorld(){
		return world;
	}
	public UHC getMain(){
		return main;
	}
	public static Game getGameFromWorld(World w){
		for(Game g : UHC.plugin.games){
			if(g.getWorld() == w){
				return g;
			}
		}
		return null;
	}
}
