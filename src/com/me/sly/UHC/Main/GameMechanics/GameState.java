package com.me.sly.UHC.Main.GameMechanics;

public enum GameState {

	PREGAME(),
	STARTUP(),
	GRACE_PERIOD(),
	INGAME(),
	PRE_DEATHMATCH(),
	DEATHMATCH(),
	END();
}
