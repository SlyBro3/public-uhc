package com.me.sly.UHC.Main;

import org.bukkit.Bukkit;

import com.me.sly.PermissionsMechanics.Listeners.PlayerRankJoin;
import com.me.sly.UHC.Main.GameMechanics.GameListeners;
import com.me.sly.UHC.Main.GameMechanics.GameLobbyListeners;
import com.me.sly.commands.GetRank;
import com.me.sly.commands.NewJoinSign;
import com.me.sly.commands.SetRank;

public class EnableMethods {

	public static void registerListeners(){
		Bukkit.getPluginManager().registerEvents(new PlayerRankJoin(), UHC.plugin);
		Bukkit.getPluginManager().registerEvents(new GameListeners(), UHC.plugin);
		Bukkit.getPluginManager().registerEvents(new GameLobbyListeners(), UHC.plugin);
		ChatAPI.consoleBroadcast("Registering Listeners...");
	}
	public static void registerCommands(){
		UHC.plugin.getCommand("GetRank").setExecutor(new GetRank());
		UHC.plugin.getCommand("SetRank").setExecutor(new SetRank());
		UHC.plugin.getCommand("NewJoinSign").setExecutor(new NewJoinSign());
		ChatAPI.consoleBroadcast("Registering Commands...");
	}
}
