package com.me.sly.UHC.Main;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.me.sly.PermissionsMechanics.Rank;
import com.me.sly.PermissionsMechanics.RankAPI;

public class ChatAPI {

	public static String TAG = "�e>>" + "�a ";
	public static String SPECIAL_TAG = "�b�l>> ";
	public static String _PREFIX = "�7[�bUHC�7] �d";
	public static String _PERMSTAG = "�7[�aPERMS�7] �c";
	public static String _NOPERMS = "�7[�aPERMS�7] �cNo permission.";
	
    public static void consoleBroadcast(final String message) {
        new BukkitRunnable() {
            public void run() {
                Bukkit.getConsoleSender().sendMessage(TAG + message);
            }
        }.runTaskLater(UHC.plugin, 20);
    }

    public static void messagePlayer(Player p, String message) {
        p.sendMessage(_PREFIX + message);
    }
	public static void cleanMessagePlayer(Player p, String message) {
		p.sendMessage(message);
	}

	/**
	 * @param message What you want to broadcast.
	 * @param rankValueNeededToSee Who can see the broadcast, put 0 for all players
	 * @param isImportant Whether or not the message is important
	 */
	public static void broadcastMessage(String message, Integer rankValueNeededToSee, boolean isImportant){
		for(World w : Bukkit.getWorlds()) {
			for (Player p : Bukkit.getWorld(w.getName()).getPlayers()) {
				Integer rank = RankAPI.getRank(p).getRankValue();
				if(rank >= rankValueNeededToSee){
					p.sendMessage(isImportant ? SPECIAL_TAG + message : TAG + message);
					p.playSound(p.getLocation(), Sound.BAT_TAKEOFF, 1F, 1F);
				}
			}
		}
	}
	public static void broadcastGameMessage(String message, World ww, boolean isImportant){
			for (Player p : Bukkit.getWorld(ww.getName()).getPlayers()) {
					p.sendMessage(isImportant ? SPECIAL_TAG + message : TAG + message);
					p.playSound(p.getLocation(), Sound.BAT_TAKEOFF, 1F, 1F);
			}
	}
	public static String getRankMessageFormat(Player player, String message){
	String _format;
	Rank _rank = RankAPI.getRank(player);
	if(!RankAPI.getRank(player).equals(Rank.USER)){
		_format = _rank.getColor() + "�l" + _rank.getFormat()
				+ _rank.getColor() + " " + player.getName() + ": "
				+ "�7" + message;
	}else{
		_format = "�7" + player.getName() + ": "
				+ message;
	}
	return _format;
}
}
