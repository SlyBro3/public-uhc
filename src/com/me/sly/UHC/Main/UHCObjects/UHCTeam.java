package com.me.sly.UHC.Main.UHCObjects;

import java.util.List;

import org.bukkit.entity.Player;

import com.me.sly.UHC.Main.GameMechanics.Game;

public class UHCTeam {
	
	private List<String> players;
	private String name;
	public UHCTeam(List<String> players, String name){
		this.players = players;
		this.name = name;
	}
	public List<String> getPlayers(){
		return players;
	}
	public String getName(){
		return name;
	}
	public void addPlayer(Player p){
		players.add(p.getName());
	}
	public static UHCTeam getTeamFor(Game g, Player p){
		for(UHCTeam t : g.getTeams()){
			if(t.getPlayers().contains(p.getName())) return t;
		}
		return null;
	}
}
