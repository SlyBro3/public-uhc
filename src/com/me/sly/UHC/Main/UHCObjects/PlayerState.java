package com.me.sly.UHC.Main.UHCObjects;

public enum PlayerState {

	FROZEN(),
	NORMAL(),
	SPECTATOR();
}
