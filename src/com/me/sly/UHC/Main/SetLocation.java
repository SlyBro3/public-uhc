package com.me.sly.UHC.Main;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.me.sly.PermissionsMechanics.RankAPI;

public class SetLocation implements CommandExecutor{
		@Override
		public boolean onCommand(CommandSender sender, Command cmd, String cmdL,
				String[] args) {
			if(sender instanceof Player){
				Player p = (Player) sender;
			if(RankAPI.getRank(p).getRankValue() > 3){
				if(cmd.getName().equalsIgnoreCase("SetLocation")){
						if(args.length == 1){
						switch(args[0]){
						case "spawn":
							UHC.plugin.getConfig().set("SpawnLoc.X", p.getLocation().getBlockX());
							UHC.plugin.getConfig().set("SpawnLoc.Y", p.getLocation().getBlockY());
							UHC.plugin.getConfig().set("SpawnLoc.Z", p.getLocation().getBlockZ());
							break;
						default:
							p.sendMessage("�7Location not found!");
								break;
						}
						UHC.plugin.saveConfig();
						UHC.plugin.reloadConfig();
					}
				}
				}
			}
			return false;
		}
	}