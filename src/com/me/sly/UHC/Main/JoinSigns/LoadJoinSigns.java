package com.me.sly.UHC.Main.JoinSigns;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.me.sly.UHC.Main.UHC;

public class LoadJoinSigns {
	public static void loadJoinSigns() {

		File file = new File(UHC.plugin.getDataFolder() + "/JoinSign", "joinsigns.yml");
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		for (String str : config.getKeys(false)) {
			ConfigurationSection s = config.getConfigurationSection(str);

			ConfigurationSection l = s.getConfigurationSection("loc");
			World w = Bukkit.getServer().getWorld(l.getString("world"));
			double x = l.getDouble("x"), y = l.getDouble("y"), z = l.getDouble("z");
			Location loc = new Location(w, x, y, z);
			UHC.plugin.joinSigns.add(new JoinSign((Sign)loc.getBlock().getState(), Bukkit.getWorld(config.getString("world"))));
		}
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void save(JoinSign joinSign) {
		File file = new File(UHC.plugin.getDataFolder() + "/JoinSign", "joinsigns.yml");
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		int size = config.getKeys(false).size() + 1;

		config.set(size + ".loc.world", joinSign.getLocation().getWorld().getName());
		config.set(size + ".loc.x", joinSign.getLocation().getX());
		config.set(size + ".loc.y", joinSign.getLocation().getY());
		config.set(size + ".loc.z", joinSign.getLocation().getZ());
		config.set(size + ".world", joinSign.getWorld());

		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void spawnJoinSigns() {
		for (JoinSign o : UHC.plugin.joinSigns) {
			o.getLocation().getWorld().getBlockAt(o.getLocation()).setType(Material.SIGN);
			Sign sign = (Sign) o.getLocation().getWorld().getBlockAt(o.getLocation()).getState(); //block has to be a sign
			sign.setLine(0, "�a[JOIN]");
			sign.setLine(1, "0 in game");
			sign.setLine(2, "8 per game");
			sign.setLine(4, "�aReady");
			
		}
	}
}
