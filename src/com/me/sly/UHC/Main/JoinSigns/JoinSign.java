package com.me.sly.UHC.Main.JoinSigns;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Sign;

import com.me.sly.UHC.Main.UHC;

public class JoinSign {

	private static Sign sign;
	private static World world;
	public JoinSign(Sign signs, World worlds){
		sign = signs;
		world = worlds;
	}
	public Sign getSign(){
		return sign;
	}
	public World getWorld(){
		return world;
	}
	public Location getLocation(){
		return sign.getLocation();
	}
	public static boolean isJoinSign(Sign sign){
		for(JoinSign c : UHC.plugin.joinSigns){
			if(c.getLocation().equals(sign.getLocation())){
				return true;
			}
		}
		return false;
	}
	public static JoinSign getJoinSignFor(Sign sign){
		if (!isJoinSign(sign)) {
			return null;
		}
		for (JoinSign c : UHC.plugin.joinSigns) {
			if (c.getLocation().equals(sign.getLocation())) {
				return c;
			}
		}
		return null;
	}
	public void updateSign(){
		sign.setLine(2, world.getPlayers().size() + " in game");
	}
}
